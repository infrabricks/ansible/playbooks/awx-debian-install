# AWX Debian install

Ansible playbook to install AWX on Debian (AWX Debian port).

## Requirements

* You need roles bellow to use this playbook :
  * [AWX Debian install](https://gitlab.mim-libre.fr/infrabricks/ansible/roles/awx-debian-install)
* Ansible >= 4

## OS

* Debian

## Playbook Example

An example of playbook

```
- name: Deploy AWX on Debian
  hosts: all
  vars_files:
    - vars/main.yml
    - vars/secret.yml
  tasks:
    - name: Install AWX
      ansible.builtin.include_role:
        name: awx_debian_install
```

## Author Information

* [Stéphane Paillet](mailto:spaillet@ethicsys.fr)
